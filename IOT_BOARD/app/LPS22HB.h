#ifndef SRC_LPS22HB_H_
#define SRC_LPS22HB_H_

#include "stm32l4xx_hal.h"

#define LPS22HB_I2C_WRITE_ADDR 0xBA
#define LPS22HB_I2C_READ_ADDR 0xBB
#define LPS22HB_WHO_AM_I 0x0F // should be 0b10110001 = 0xB1

#define LPS22HB_CTRL_REG1 0x10
#define LPS22HB_CTRL_REG2 0x11

#define LPS22HB_STATUS 0x27

#define LPS22HB_PRESS_OUT_XL 0x28
#define LPS22HB_PRESS_OUT_L 0x29
#define LPS22HB_PRESS_OUT_H 0x2A

#define LPS22HB_TEMP_OUT_L 0x2B
#define LPS22HB_TEMP_OUT_H 0x2C


class LPS22HB {
public:
  LPS22HB(I2C_HandleTypeDef& i2c);
  virtual ~LPS22HB();
  void init();
  uint32_t getPressure();
  float getTemperature();

  inline uint8_t whoAmI() const {
    return WhoAmI;
  }

private:
  uint8_t read(uint8_t address);
  void write(uint8_t address, uint8_t value);

  I2C_HandleTypeDef& I2C;
  uint8_t I2CBuffer[2];

  uint8_t WhoAmI;
  uint32_t Pressure;
  uint16_t Temperature;
};

#endif /* SRC_LPS22HB_H_ */
