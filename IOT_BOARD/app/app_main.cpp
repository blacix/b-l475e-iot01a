#include <OsThread.h>
#include <ProducerThread.h>
#include <SensorThread.h>
#include <UartThread.h>
#include <app_main.h>
#include <app_typedefs.h>
#include <stdio.h>
#include <string.h>

#include "i2c.h"
#include "usart.h"


// from app_os_defs.c
extern const osThreadAttr_t uartThread_attributes;
extern const osThreadAttr_t defaultThread_attributes;
extern const osThreadAttr_t producerThread_attributes;

osEventFlagsId_t producerFlag;
osEventFlagsId_t uartFlag;
osMessageQueueId_t messageQueue;


UartThread uartThread(huart1, messageQueue, uartFlag);
osThreadId_t uartThreadId;

SensorThread sensorThread(hi2c2, messageQueue);
osThreadId_t sensorThreadId;


ProducerThread producerThread1(messageQueue, producerFlag);
osThreadId_t producerThreadId1;
ProducerThread producerThread2(messageQueue, producerFlag);
osThreadId_t producerThreadId2;


/**
 * @brief  FreeRTOS initialization
 * @param  None
 * @retval None
 */
void MX_FREERTOS_Init(void) {
  producerFlag = osEventFlagsNew(NULL);
  uartFlag = osEventFlagsNew(NULL);
  messageQueue = osMessageQueueNew(10, sizeof(MSGQUEUE_OBJ_t), NULL);

  producerThreadId1 = osThreadNew(StartOsThread, &producerThread1, &producerThread_attributes);

  uartThreadId = osThreadNew(StartOsThread, &uartThread, &uartThread_attributes);
  // sensorThreadId = osThreadNew(StartOsThread, &sensorThread, &defaultThread_attributes);
}


// interrupt handler for user button ( blue )
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin) {
  static int iqrCntr = 0;
  // TODO: HACK, IT is fired on startup, check cube
  if (iqrCntr < 1) {
    iqrCntr++;
    return;
  }

  // set flag for PorducerThread
  osEventFlagsSet(producerFlag, FLAG_MASK_PRODUCER);
  HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_14);
}


// called when UART reception is ready in interrupt mode
void HAL_UART_RxCpltCallback(UART_HandleTypeDef* huart) {
  if (huart == &huart1) {
    // uart1 finished with receiving
    // set flag for UartThread
    osEventFlagsSet(uartFlag, FLAG_MASK_UART_RX);
  }
}
