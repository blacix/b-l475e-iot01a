#ifndef OS_TIMER_H
#define OS_TIMER_H


#include "cmsis_os.h"

#ifdef __cplusplus

enum class TimerEvent {
  Event1,Event2
};

class TimerCallback {
public:
  virtual void timeout(TimerEvent event) = 0;
};

class OsTimer {
public:
  OsTimer(TimerEvent event, TimerCallback& callback);
  OsTimer(const OsTimer&&) = delete;
  virtual ~OsTimer() = default;
  void start();
  // called in different context
  // check RTOS doc on timers
  void timeout(TimerEvent event);

  TimerEvent getEvent() { return mEvent; }


private:
  osTimerId_t mHandle;
  TimerEvent mEvent;
  TimerCallback& mCallback;
};


extern "C" {

#endif

void OsTimerTimeout(void* argument);

#ifdef __cplusplus
}
#endif

#endif /* OS_TIMER_H */
