#include "cmsis_os2.h"

// .stack_size = 128, too small
const osThreadAttr_t uartThread_attributes
    = {.name = "uartThread", .priority = (osPriority_t)osPriorityNormal, .stack_size = 1024};


const osThreadAttr_t producerThread_attributes
    = {.name = "producerThread", .priority = (osPriority_t)osPriorityNormal, .stack_size = 512};


const osThreadAttr_t defaultThread_attributes
    = {.name = "defaultThread", .priority = (osPriority_t)osPriorityNormal, .stack_size = 512};


/* Definitions for myTimer01 */
const osTimerAttr_t myTimer01_attributes = {
  .name = "myTimer01"
};

