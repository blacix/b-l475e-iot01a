#ifndef SENSORTHREAD_H_
#define SENSORTHREAD_H_
#include <HTS221.h>
#include <LPS22HB.h>
#include <OsThread.h>
#include <app_typedefs.h>
#include <cmsis_os.h>

class SensorThread : public OsThread {
public:
  SensorThread(I2C_HandleTypeDef& i2c, osMessageQueueId_t& messageQueue);
  virtual ~SensorThread() = default;
  virtual void loop() override;

private:
  HTS221 HTS221Sensor;
  LPS22HB LPS22HBSensor;
  osMessageQueueId_t& MessageQueue;
  MSGQUEUE_OBJ_t MessageObj;
};

#endif /* SENSORTHREAD_H_ */
