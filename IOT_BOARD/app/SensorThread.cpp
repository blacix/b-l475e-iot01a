#include <SensorThread.h>
#include <stdio.h>
#include <stm32l4xx_hal.h>


SensorThread::SensorThread(I2C_HandleTypeDef& i2c, osMessageQueueId_t& messageQueue) :
    HTS221Sensor(i2c),
    LPS22HBSensor(i2c),
    MessageQueue(messageQueue) {}

void SensorThread::loop() {
  HTS221Sensor.init();
  sprintf((char*)MessageObj.Buf, "HTS221: 0x%X\n", HTS221Sensor.whoAmI());
  osMessageQueuePut(MessageQueue, &MessageObj, 0U, 0U);

  LPS22HBSensor.init();
  sprintf((char*)MessageObj.Buf, "LPS22HB: 0x%X\n", LPS22HBSensor.whoAmI());
  osMessageQueuePut(MessageQueue, &MessageObj, 0U, 0U);


  for (;;) {
    int t1 = HTS221Sensor.getTemperature() * 100;
    int t2 = LPS22HBSensor.getTemperature() * 100;
    sprintf((char*)MessageObj.Buf,
            "T1: %d.%.2d C | T2: %d.%.2d C | rH: %d%% | P: %lu hPa\n",
            t1 / 100,
            t1 % 100,
            t2 / 100,
            t2 % 100,
            (int)HTS221Sensor.getHumidity(),
            LPS22HBSensor.getPressure());

    osMessageQueuePut(MessageQueue, &MessageObj, 0U, 0U);
    osDelay(1000);
  }
}
