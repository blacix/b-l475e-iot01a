#ifndef OS_THREAD_H
#define OS_THREAD_H


#ifdef __cplusplus

class OsThread {
public:
  OsThread() = default;
  OsThread(const OsThread&&) = delete;
  virtual ~OsThread() = default;
  virtual void loop() = 0;


private:
};


extern "C" {

#endif

void StartOsThread(void* argument);


#ifdef __cplusplus
}
#endif

#endif /* OS_THREAD_H */
