#ifndef UART_THREAD_H
#define UART_THREAD_H

#include "cmsis_os.h"
#include "stm32l4xx_hal.h"


#include "OsThread.h"

#ifdef __cplusplus

class UartThread : public OsThread {
public:
  UartThread(UART_HandleTypeDef& uartHandle, osMessageQueueId_t& messageQueue, osEventFlagsId_t& uartFlag);
  virtual ~UartThread() = default;
  void loop() override;

private:

  void send(uint8_t* const& data, size_t size);

  UART_HandleTypeDef& UartHandle;
  osMessageQueueId_t& MessageQueue;
  osEventFlagsId_t& UartFlag;
};


extern "C" {

#endif


#ifdef __cplusplus
}
#endif

#endif // UART_THREAD_H
