
#ifndef INC_APP_MAIN_H_
#define INC_APP_MAIN_H_

#include <inttypes.h>

#include "cmsis_os.h"


#ifdef __cplusplus
extern "C" {
#endif

void MX_FREERTOS_Init(void);


#ifdef __cplusplus
}
#endif


#endif /* INC_APP_MAIN_H_ */
