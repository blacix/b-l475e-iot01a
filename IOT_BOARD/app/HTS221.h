#ifndef HTS221_H_
#define HTS221_H_

#include "stm32l4xx_hal.h"


#define HTS221_I2C_WRITE_ADDR 0xBE
#define HTS221_I2C_READ_ADDR 0xBF

#define HTS221_WHO_AM_I 0x0F

#define HTS221_AV_CONF 0x10

#define HTS221_CTRL_REG1 0x20
#define HTS221_CTRL_REG2 0x21

// data registers
#define HTS221_HUMIDITY_OUT_L 0x28
#define HTS221_HUMIDITY_OUT_H 0x29

#define HTS221_TEMP_OUT_L 0x2A
#define HTS221_TEMP_OUT_H 0x2B

// calibration registers
#define HTS221_H0_RH_X2 0x30 // 30 H0_rH_x2 (u8) H0.7 H0.6 H0.5 H0.4 H0.3 H0.2 H0.1 H0.1
#define HTS221_H1_RH_X2 0x31 // 31 H1_rH_x2 (u8) H1.7 H1.6 H1.5 H1.4 H1.3 H1.2 H1.1 H1.0

#define HTS221_T0_DEGC_x8 0x32 // 32 T0_degC_x8 (u8) T0.7 T0.6 T0.5 T0.4 T0.3 T0.2 T0.1 T0.0
#define HTS221_T1_DEGC_x8 0x33 // 33 T1_degC_x8 (u8) T1.7 T1.6 T1.5 T1.4 T1.3 T1.2 T1.1 T1.0

#define HTS221_T1_T0_MSB 0x35 // 35 T1/T0 msb (u2),(u2) Reserved T1.9 T1.8 T0.9 T0.8

#define HTS221_H0_T0_OUT_L 0x36
#define HTS221_H0_T0_OUT_H 0x37

#define HTS221_H1_T0_OUT_L 0x3A
#define HTS221_H1_T0_OUT_H 0x3B

#define HTS221_T0_OUT_L 0x3C
#define HTS221_T0_OUT_H 0x3D

#define HTS221_T1_OUT_L 0x3E
#define HTS221_T1_OUT_H 0x3F


class HTS221 {
public:
  HTS221(I2C_HandleTypeDef& i2c);
  virtual ~HTS221();
  void init();
  inline uint8_t whoAmI() const {
    return WhoAmI;
  }
  void readCalibrationData();
  float getTemperature();
  float getHumidity();

private:
  uint8_t read(uint8_t address);
  void write(uint8_t address, uint8_t value);

  I2C_HandleTypeDef& I2C;
  uint8_t I2CBuffer[2];

  uint8_t WhoAmI;

  // humidity
  uint16_t H_out;
  uint8_t H0_rh_x2;
  uint8_t H1_rh_x2;
  uint16_t H0_T0_out;
  uint16_t H1_T0_out;
  float Humidity;

  // temperature
  uint16_t T_out;
  uint16_t T0_out;
  uint16_t T1_out;
  uint16_t T0_degC;
  uint16_t T1_degC;
  float T_DegC;
};

#endif /* HTS221_H_ */
