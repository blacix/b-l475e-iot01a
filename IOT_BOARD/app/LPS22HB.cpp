#include "LPS22HB.h"

LPS22HB::LPS22HB(I2C_HandleTypeDef& i2c) : I2C(i2c), WhoAmI(0), Pressure(0), Temperature(0) {}

LPS22HB::~LPS22HB() {}

void LPS22HB::init() {
  WhoAmI = read(LPS22HB_WHO_AM_I);
  write(LPS22HB_CTRL_REG1, 0x00); // one shot mode - power down
}

uint32_t LPS22HB::getPressure() {
  write(LPS22HB_CTRL_REG2, 0x01); // one shot
  uint8_t press_out_xl = read(LPS22HB_PRESS_OUT_XL);
  uint8_t press_out_l = read(LPS22HB_PRESS_OUT_L);
  uint8_t press_out_h = read(LPS22HB_PRESS_OUT_H);

  // 24 bit number
  Pressure = (press_out_h << 16 | press_out_l << 8 << press_out_xl) & 0xFFFFFF;
  // value/4096 = hPa value
  Pressure = Pressure >> 12;

  return Pressure;
}

float LPS22HB::getTemperature() {
  write(LPS22HB_CTRL_REG2, 0x01); // one shot
  uint8_t temp_out_l = read(LPS22HB_TEMP_OUT_L);
  uint8_t temp_out_h = read(LPS22HB_TEMP_OUT_H);

  Temperature = temp_out_h << 8 | temp_out_l;

  return Temperature / 100.0f;
}

uint8_t LPS22HB::read(uint8_t address) {
  I2CBuffer[0] = address;
  HAL_I2C_Master_Transmit(&I2C, LPS22HB_I2C_READ_ADDR, I2CBuffer, 1, HAL_MAX_DELAY);
  HAL_I2C_Master_Receive(&I2C, LPS22HB_I2C_READ_ADDR, I2CBuffer, 1, HAL_MAX_DELAY);
  return I2CBuffer[0];
}

void LPS22HB::write(uint8_t address, uint8_t value) {
  I2CBuffer[0] = address;
  I2CBuffer[1] = value;
  HAL_I2C_Master_Transmit(&I2C, LPS22HB_I2C_WRITE_ADDR, I2CBuffer, 2, HAL_MAX_DELAY);
}
