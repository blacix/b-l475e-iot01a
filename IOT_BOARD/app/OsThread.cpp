#include <OsThread.h>
#include <string.h>

#include "cmsis_os.h"


void StartOsThread(void* argument) {
  OsThread* task = static_cast<OsThread*>(argument);
  task->loop();
}
