#ifndef INC_APP_TYPEDEFS_H_
#define INC_APP_TYPEDEFS_H_

#include <inttypes.h>

typedef struct {
  uint8_t Buf[128];
  uint8_t Idx;
} MSGQUEUE_OBJ_t;

constexpr uint32_t FLAG_MASK_UART_RX = 0x00000001;
constexpr uint32_t FLAG_MASK_PRODUCER = 0x00000001;

#endif /* INC_APP_TYPEDEFS_H_ */
