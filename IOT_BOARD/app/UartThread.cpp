#include <UartThread.h>
#include <app_typedefs.h>
#include <string.h>


UartThread::UartThread(UART_HandleTypeDef& uartHandle, osMessageQueueId_t& messageQueue, osEventFlagsId_t& uartFlag) :
    UartHandle(uartHandle),
    MessageQueue(messageQueue),
    UartFlag(uartFlag) {}

void UartThread::loop() {
  MSGQUEUE_OBJ_t msg;
  osStatus_t status;
  uint8_t rxBuffer[64];

  for (;;) {

    // wait for a message from other tasks, sends data over uart
    status = osMessageQueueGet(MessageQueue, &msg, NULL, 10 /*osWaitForever*/);
    if (status == osOK) {
      send(msg.Buf, strlen(reinterpret_cast<char*>(msg.Buf)));
    }

    // rx 10 bytes to localBuff
    HAL_UART_Receive_IT(&UartHandle, rxBuffer, 10);

    // flag set in HAL_UART_RxCpltCallback when uart rx is finished
    if ((osEventFlagsGet(UartFlag) & FLAG_MASK_UART_RX) > 0) {
      // send back what is received via uart
      send(rxBuffer, 10);
      // clear the flag and wait for new message
      osEventFlagsClear(UartFlag, FLAG_MASK_UART_RX);
    }

    // osDelay(100);
  }
}


void UartThread::send(uint8_t* const& data, size_t size) {
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_14, GPIO_PIN_SET);
  HAL_UART_Transmit(&UartHandle, data, size, HAL_MAX_DELAY);
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_14, GPIO_PIN_RESET);
}
