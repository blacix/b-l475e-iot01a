#include <ProducerThread.h>
#include <app_typedefs.h>
#include <stdio.h>
#include <string.h>


ProducerThread::ProducerThread(osMessageQueueId_t& messageQueue, osEventFlagsId_t& eventFlag) :
    MessageQueue(messageQueue),
    EventFlag(eventFlag) {}


void ProducerThread::loop() {
  MSGQUEUE_OBJ_t msg;
  sprintf((char*)msg.Buf, "%s%u\n", "producer:", reinterpret_cast<unsigned int>(&msg));

  // waits for flag from user button interrupt
  osEventFlagsWait(EventFlag, 0x0001, osFlagsWaitAny, osWaitForever);

  for (;;) {
    // wait for flag from defaultTask
    // osThreadFlagsWait(0x01U, osFlagsWaitAny, osWaitForever);
    osMessageQueuePut(MessageQueue, &msg, 0U, 0U);
    osDelay(500);
  }
}
