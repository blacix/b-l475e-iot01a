#include <HTS221.h>

HTS221::HTS221(I2C_HandleTypeDef& i2c) : I2C(i2c) {}

HTS221::~HTS221() {}


void HTS221::init() {
  // PD 1, BDU 1, ODR 7hz
  write(HTS221_CTRL_REG1, 0b10000110);

  // PD - one shot
  //	write(HTS221_CTRL_REG1, 0x80);

  // whoami
  WhoAmI = read(HTS221_WHO_AM_I);

  readCalibrationData();
}


void HTS221::readCalibrationData() {
  // Hx_rh_x2
  H0_rh_x2 = read(HTS221_H0_RH_X2);
  H0_rh_x2 = H0_rh_x2 >> 1; // /2

  H1_rh_x2 = read(HTS221_H1_RH_X2);
  H1_rh_x2 = H1_rh_x2 >> 1; // /2


  // Tx_DegC
  uint8_t t0_degC_x8 = read(HTS221_T0_DEGC_x8);
  uint8_t t1_degC_x8 = read(HTS221_T1_DEGC_x8);
  uint8_t t0_t1_msb = read(HTS221_T1_T0_MSB);
  uint8_t t0_msb = t0_t1_msb & 0x03;
  uint8_t t1_msb = (t0_t1_msb >> 2) & 0x03;

  T0_degC = t0_msb << 8 | t0_degC_x8;
  T0_degC = T0_degC >> 3; // /8

  T1_degC = t1_msb << 8 | t1_degC_x8;
  T1_degC = T1_degC >> 3;


  // Hx_T0_OUT
  uint8_t h0_t0_out_l = read(HTS221_H0_T0_OUT_L);
  uint8_t h0_t0_out_h = read(HTS221_H0_T0_OUT_H);
  H0_T0_out = h0_t0_out_h << 8 | h0_t0_out_l;

  uint8_t h1_t0_out_l = read(HTS221_H1_T0_OUT_L);
  uint8_t h1_t0_out_h = read(HTS221_H1_T0_OUT_H);
  H1_T0_out = h1_t0_out_h << 8 | h1_t0_out_l;


  // Tx_out
  uint8_t t0_out_l = read(HTS221_T0_OUT_L);
  uint8_t t0_out_h = read(HTS221_T0_OUT_H);
  T0_out = t0_out_h << 8 | t0_out_l;

  uint16_t t1_out_l = read(HTS221_T1_OUT_L);
  uint16_t t1_out_h = read(HTS221_T1_OUT_H);
  T1_out = t1_out_h << 8 | t1_out_l;
}


float HTS221::getTemperature() {
  // PD - one shot
  //	write(HTS221_CTRL_REG1, 0x80);
  //	write(HTS221_CTRL_REG2, 0x01); //one shot


  uint8_t t_out_l = read(HTS221_TEMP_OUT_L);
  uint8_t t_out_h = read(HTS221_TEMP_OUT_H);
  T_out = t_out_h << 8 | t_out_l;

  T_DegC = T0_degC + (float)(T_out - T0_out) * (T1_degC - T0_degC) / (T1_out - T0_out);

  return T_DegC;
}

float HTS221::getHumidity() {
  // PD - one shot
  //	write(HTS221_CTRL_REG1, 0x80);
  //	write(HTS221_CTRL_REG2, 0x01);

  uint8_t h_out_l = read(HTS221_HUMIDITY_OUT_L);
  uint8_t h_out_h = read(HTS221_HUMIDITY_OUT_H);
  H_out = h_out_h << 8 | h_out_l;

  Humidity = H0_rh_x2 + (H_out - H0_T0_out) * (H1_rh_x2 - H0_rh_x2) / (H1_T0_out - H0_T0_out);

  return Humidity;
}

uint8_t HTS221::read(uint8_t address) {
  I2CBuffer[0] = address;
  HAL_I2C_Master_Transmit(&I2C, HTS221_I2C_READ_ADDR, I2CBuffer, 1, HAL_MAX_DELAY);
  HAL_I2C_Master_Receive(&I2C, HTS221_I2C_READ_ADDR, I2CBuffer, 1, HAL_MAX_DELAY);
  return I2CBuffer[0];
}

void HTS221::write(uint8_t address, uint8_t value) {
  I2CBuffer[0] = address;
  I2CBuffer[1] = value;
  HAL_I2C_Master_Transmit(&I2C, HTS221_I2C_WRITE_ADDR, I2CBuffer, 2, HAL_MAX_DELAY);
}


//		int x;
//		static_cast<char*>(static_cast<void*>(&x));
//     ret = HAL_I2C_Master_Receive(&I2C, HTS221_I2C_ADDRESS, reinterpret_cast<uint8_t*>(&h1_rh_x2), 1, HAL_MAX_DELAY);
