#ifndef PRODUCER_THREAD_H
#define PRODUCER_THREAD_H

#include <OsThread.h>

#include "cmsis_os.h"

class ProducerThread : public OsThread {
public:
  ProducerThread(osMessageQueueId_t& messageQueue, osEventFlagsId_t& eventFlag);
  virtual ~ProducerThread() = default;

  void loop() override;

private:
  osMessageQueueId_t& MessageQueue;
  osEventFlagsId_t& EventFlag;
};

#endif // PRODUCER_THREAD_H
