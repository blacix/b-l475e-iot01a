#include "OsTimer.h"


// TODO
extern const osTimerAttr_t myTimer01_attributes;


void OsTimerTimeout(void* argument) {
  OsTimer* timer = static_cast<OsTimer *>(argument);
  timer->timeout(timer->getEvent());
}


OsTimer::OsTimer(TimerEvent event, TimerCallback& callback) : mEvent(event), mCallback(callback) {

}

void OsTimer::start() {
  mHandle = osTimerNew(OsTimerTimeout, osTimerPeriodic, this, &myTimer01_attributes);
  osTimerStart(mHandle, 500U);
}

void OsTimer::timeout(TimerEvent event) {
  mCallback.timeout(event);
}

